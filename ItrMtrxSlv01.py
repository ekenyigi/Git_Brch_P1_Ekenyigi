# load library
import numpy as np
import time as tm

# give the entries in matrix A and column vector f
A = np.array([[-3, 1, 2],[-1, -3, 1],[-1, 3, 4]])  

f = np.array([2, 6, 4])

# decompose A to D and R matrices
D = np.zeros((3,3))

R = np.zeros((3,3))

for i in range(3):
    for j in range(3):

        if i == j:
            D[i][j] = A[i][j]
        else:
            R[i][j] = A[i][j]


## Test add a line to the file in branch.

##Does this added

# inverse of D matrix

Dinv = np.linalg.inv(D)

# initial guess value of x
x = np.array([0, 0, 0])

# walltime before matrix solving
cpu_tm_bg = tm.time()

nrm = [] 

T = np.zeros(3)

# iteration
for iter in range(200):
    # use function np.matmul for matrix-vector multiplication
        
    T = np.matmul(Dinv,(f - np.matmul(R,T)))

    # residual during iterations
    
    resd = f - np.matmul(A, T)
    
    # use calculate the norm of the residual using numpy function

    L_two = (resd[0]**2 + resd[1]**2 + resd[2]**2) ** 0.5 

    nrm.append(L_two)

    if nrm[-1] < 1e-5:
        break

    

# walltime after matrix solving
cpu_tm_ed = tm.time()    

# print results
print(cpu_tm_ed - cpu_tm_bg, nrm)

